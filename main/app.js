function distance(first, second) {
	if (!(Array.isArray(first) && Array.isArray(second)))
		throw new Error("InvalidType");

	if (first == null || second == null)
		return 0;

	for (var i = 0; i < first.length; i++) {
		if (!isNaN(first[i]))
			return 2;
	}

	for (var i = 0; i < second.length; i++) {
		if (!isNaN(second[i]))
			return 2;
	}

	var seen = {};
	var resF = [];
	var j = 0;

	for (var i = 0; i < first.length; i++) {
		var item = first[i];
		if (seen[item] !== 1) {
			seen[item] = 1;
			resF[j++] = item;
		}
	}

	var seenS = {};
	var resS = [];
	var k = 0;

	for (var i = 0; i < second.length; i++) {
		var item = second[i];
		if (seenS[item] !== 1) {
			seenS[item] = 1;
			resS[k++] = item;
		}
	}

	var count = 0;
	var flag = false;


	if (resS.length > resF.length) {
		var a = resS;
		var b = resF;
	} else {
		var a = resF;
		var b = resS;
	}
	for (var i = 0; i < a.length; i++) {
		flag = false;
		for (var j = 0; j < b.length; j++) {
			if (a[i] == b[j]) {
				flag = true;
			}
		}

		if (flag == false) {
			count++;
		}
	}

	return count;
}


module.exports.distance = distance